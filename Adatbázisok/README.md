# Adatbázisok

Témakörök

1. Adatbáziskezelő rendszerek feladatai, felépítése; Elvi felépítés, háromrétegű modell; Járulékos feladatok: integritás, szinkronizálás, adatbiztonság
2. Fizikai adatszervezés módszerei; Heap szervezés; Hash-állományok; Indexelt állományok (ritka egyszintű, ritka többszintű, sűrű); Invertálás, változó hosszúságú rekordok kezelése
3. ER modell és diagram; Egyedhalmazok, tulajdonsághalmazok, Specializáció/általánosítás, gyenge egyedhalmazok
4. Relációs adatmodell; Az adatok strukturálása; Relációalgebra, illesztések
5. Relációs sématervezés ER diagramból
6. Normál formák; Adatbázis kényszerek, funkcionális függések, redundancia, reláció redundanciája, 0NF, 1NF,2NF,3NF, BCNF
7. SQL nyelv; Táblák, nézetek létrehozása, lekérdezések, táblák módosítása, adatok módosítása, adatelérések szabályozása, kényszerek kezelése
8. Relációs lekérdezések optimalizálása; Heurisztikus optimalizálás, ekvivalens kifejezések; Költség alapú optimalizálás, költségbecslések
9. Tranzakciókezelés; Ütemezések, tranzakciókezelés zárakkal, 2PL, hierarchikus adategységek zárolása, szigorú protokollok, helyreállítás rendszerhibák után, időbélyeges tranzakciókezelés, verziókezelés alapú tranzakciókezelés
