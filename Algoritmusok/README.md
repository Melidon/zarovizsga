# Algoritmusok

Témakörök

1. [Lineáris és bináris keresés; Buborék-, beszúrásos, összefésüléses rendezés, gyorsrendezés, alsó becslés az összehasonlítások számára, ládarendezés, radix rendezés](1.md)
2. [Alapvető adatszerkezetek és elemzésük; Bináris keresőfa, 2-3 fa, B-fa, hash-tábla.](2.md)
3. [Gráfalgoritmusok; Szélességi és mélységi bejárás, alkalmazások (összefüggőség, aciklikusság, topologikus sorrend meghatározása), maximális párosítás páros gráfban (magyar módszer), Kruskal-algoritmus minimális költségű feszítőfa meghatározására, Dijksra-, Bellman-Ford-, Floyd-algoritmus legrövidebb utak meghatározására.](3.md)
4. [A bonyolultságelmélet elemei; P és NP problémaosztályok, NP-teljesség, visszavezetések, nevezetes NP-teljes problémák.](4.md)
5. [Általános algoritmustervezési módszerek; Oszd meg és uralkodj, elágazás és korlátozás, dinamikus programozás.](5.md)
6. [Egyszerű közelítő algoritmusok ; Független élek, ládapakolás feladat, utazóügynök-probléma.](6.md)

Hivatalos jegyzet:

- [BSz2](http://cs.bme.hu/bsz2/bsz2_jegyzet.pdf)

Előadás felvételek:

- [BSz2 1-6](https://web.microsoftstream.com/user/df2e2bd6-1dfb-41d9-b380-af4cb5adab17)
- [BSz2 7-12](https://web.microsoftstream.com/user/dbd02158-6e52-4245-a777-505abf3d0d07)
- [Algel](https://web.microsoftstream.com/user/61f2441f-d0b8-4efe-9e52-0a7f062ee906)

Amúgy találtam [egy másik követelményes kiírást](http://cs.bme.hu/%7Efriedl/alg/zvtem.pdf) is. Nagyjábó ugyanaz, mint ami elérhető a hivatalos kiírásban, csak egy kicsit olvashatóbb talán.
